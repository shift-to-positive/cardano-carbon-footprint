import { createApp } from "vue";
import { createPinia } from "pinia";
import { createAuth0 } from '@auth0/auth0-vue';
import App from "./App.vue";

const app = createApp(App);

app.use(createPinia());

app.use(
    createAuth0({
        domain: "dev-cardano.eu.auth0.com",
        client_id: "N0CiHfwq4BQRfduvlSyQXJRUvqJ4cJ2u",
        //redirect_uri: window.location.origin
        redirect_uri: "https://co2.stakeshift.team/"
    })
);

app.mount("#app");
