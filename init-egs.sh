#!/bin/bash
# examples and sources for card-c02

SRC=$PWD
[ -d ${SRC}/examples ]                  || mkdir ${SRC}/examples
cd ${SRC}/examples

echo pulling example repos in $PWD
[ -d grafana-infrastructure ]           || git clone https://github.com/annea-ai/grafana-infrastructure
[ -d VENoM-Docker ]                     || git clone https://github.com/jamesaud/VENoM-Docker
[ -d docker-images ]                    || git clone https://github.com/nystudio107/docker-images.git
[ -d vitejs-docker-dev ]                || git clone https://github.com/nystudio107/vitejs-docker-dev.git
[ -d vitesse ]                          || git clone https://github.com/antfu/vitesse.git
[ -d mariadb-docker ]                   || git clone https://github.com/MariaDB/mariadb-docker
[ -d nodejs-vuejs-mysql-boilerplate ]   || git clone https://github.com/chrisleekr/nodejs-vuejs-mysql-boilerplate.git
[ -d express ]                          || git clone https://gitlab.com/gitlab-org/project-templates/express
