#!/bin/bash
# run-me.sh

# standard locals
alias cd='builtin cd'
P="$(realpath -e $0)"
USAGE="`basename ${P}` [-h(elp)] [-d(ebug)] [-s(top me)] [-c(hanges)] [-r(ecreate)] [-D(ev mode)]"
DBG=:
OPTIONSTRING=hdscrD
R='\033[0;31m' # red (use with echo -e)
G='\033[0;32m' # green
B='\033[1;34m' # blue
Y='\033[0;93m' # yellow
M='\033[0;95m' # magenta
C='\033[0;96m' # cyan
N='\033[0m'    # no color

# specific locals
unset STOP_ME
unset CHANGES
unset RECREATE
unset DEV_MODE

# message & exit if exit num present
e() { (
  c=""; case $1 in R|G|B|Y|M|C|N) c=${!1}; shift; ;; esac; echo -e "${c}$*${N}";
) }
usage() { e G "Usage: $USAGE"; [ ! -z "$1" ] && exit $1; }

# process options
while getopts $OPTIONSTRING OPTION
do
  case $OPTION in
    h)	usage 0 ;;
    d)	DBG=echo ;;
    s)	STOP_ME=y ;;
    c)	CHANGES=y ;;
    r)	RECREATE=y ;;
    D)	DEV_MODE=y ;;
    *)	usage 1 ;;
  esac
done
shift `expr $OPTIND - 1`

# trigger recreation of containers after stop
COMPOSE_OPTS=""
[ x${RECREATE} == xy ] && COMPOSE_OPTS="--build --force-recreate"

# show changes mode
if [ x${CHANGES} == xy ]
then
  # do a diff with the example seeds
  e B "diffing grafana and examples/grafana-infrastructure..."
  diff -r examples/grafana-infrastructure grafana
  echo
  #e B "diffing vitesse and examples/vitesse..."
  #diff -r examples/vitesse vitesse
  exit 0
fi

# we're in default mode, so set up container run or stop or rebuild...
# container names
GF_CNTR_NAME=grafana
MARIA_CNTR_NAME=maria
PHPMYADMIN_CNTR_NAME=phpmyadmin
VUE_CNTR_NAME=ccc
ALL_CNTRS="$GF_CNTR_NAME $MARIA_CNTR_NAME $VUE_CNTR_NAME $PHPMYADMIN_CNTR_NAME"

# credentials
cd $(dirname $0)
CREDS=./private.sh
[ -f $CREDS ] && source $CREDS

# stop running containers if they exist
for CNTR in $ALL_CNTRS
do
  CNTR_ID=$(docker container ps -q --filter 'name='${CNTR})
  if [ ! -z "${CNTR_ID}" ]
  then
    echo "$CNTR running as $CNTR_ID; stopping..."
    docker stop $CNTR_ID
  fi
done

# if we're only doing stop, then we're done
[ x$STOP_ME == xy ] && echo done && exit 0

# run grafana/db/admin containers
echo "doing compose ups..."
(cd grafana/infrastructure; docker-compose up -d ${COMPOSE_OPTS})
(cd grafana/grafana; docker-compose up -d ${COMPOSE_OPTS})

# run node/express in dev mode or the CCC container
if [ x${DEV_MODE} == xy ]
then
  e Y "doing npm/pnpm in CCC/..."
  cd CCC
  npm install -g pnpm
  pnpm install --dev
  pnpm install
  pnpm run dev
else
  (
    cd CCC
    docker-compose up -d ${COMPOSE_OPTS}
    docker cp $VUE_CNTR_NAME:/app/dist .
  )
fi

# tell the world
echo "containers running:"
docker container ls
# TODO to turn off auto restart:  docker update --restart=no $(docker ps -a -q)
