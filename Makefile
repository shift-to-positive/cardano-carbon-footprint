###########################################################################
# gitlab pages and CI/CD Makefile
###########################################################################

# default: regenerate the containers and the static site
default:
	rm -rf CCC/dist
	./run-me.sh -r
	cd CCC && rm -rf site/* && cp -a dist/* site
	@echo && echo now push the changes... && echo
	git status

# deploy
# note that we add script to do redirect if we're not on co2.stakeshift.team
# avoid publishing at both gitlab.io and co2.stakeshift.team
deploy:
	rm -rf public
	cp -a CCC/site public
	sed -i '/^<head>$$/a <script> if(window.location.hostname != "co2.stakeshift.team") { window.location = "https://co2.stakeshift.team" } </script>' public/index.html
