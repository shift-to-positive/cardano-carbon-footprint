#!/bin/bash
# metadata sources

# standard locals
alias cd='builtin cd'
P="$(realpath -e $0)"
USAGE="`basename ${P}` [-h(elp)] [-d(ebug)] [-n(no xyz)] [-i [1|2|3|...]"
DBG=:
OPTIONSTRING=hdni:
R='\033[0;31m' # red (use with echo -e)
G='\033[0;32m' # green
B='\033[1;34m' # blue
Y='\033[0;93m' # yellow
M='\033[0;95m' # magenta
C='\033[0;96m' # cyan
N='\033[0m'    # no color

# specific locals
SHIFT=90dd0a60

# message & exit if exit num present
e() { (
  c=""; case $1 in R|G|B|Y|M|C|N) c=${!1}; shift; ;; esac; echo -e "${c}$*${N}";
) }
usage() { e G "Usage: $USAGE"; [ ! -z "$1" ] && exit $1; }

# process options
while getopts $OPTIONSTRING OPTION
do
  case $OPTION in
    h)	usage 0 ;;
    d)	DBG=echo ;;
    n)	USEXYZ="" ;;
    i)	ABC="${OPTARG}" ;;
    *)	usage 1 ;;
  esac
done 
shift `expr $OPTIND - 1`

# colours example
colours() {
  echo -e \
    "white ${R}red ${G}green ${B}blue ${Y}yellow ${M}magenta ${C}cyan ${N}"
}

############################################################################
# sources

e G koios
KOIOS=/tmp/koios_pool_list.txt
>${KOIOS}
curl -sX GET "https://api.koios.rest/api/v0/pool_list" -H "Accept: application/json" -H "Range: 0-999" >>${KOIOS}
curl -sX GET "https://api.koios.rest/api/v0/pool_list" -H "Accept: application/json" -H "Range: 1000-1999" >>${KOIOS}
curl -sX GET "https://api.koios.rest/api/v0/pool_list" -H "Accept: application/json" -H "Range: 2000-2999" >>${KOIOS}
curl -sX GET "https://api.koios.rest/api/v0/pool_list" -H "Accept: application/json" -H "Range: 3000-3999" >>${KOIOS}
e Y "SHIFT:\t\t${G}$(grep -i SHIFT ${KOIOS} |xargs)"
e Y "cardinality:\t${G}$(wc -l ${KOIOS})"
echo

e G adapools pools.fair.json
e Y "cardinality:\t${G}$(wget -qO - https://js.adapools.org/pools.fair.json | jq |grep '"id": "[0-9][0-9]*"'  |wc -l)"
echo

e G adapools pools.json
e Y "cardinality\t${G}$(wget -qO - https://js.adapools.org/pools.json | jq |grep '"id": "[0-9][0-9]*"'  |wc -l)"
echo

e G adapools groups.json
e Y "cardinality\t${G}$(wget -qO - https://js.adapools.org/groups.json | jq |grep '"group":' |sort |uniq |wc -l)"
echo
